<?php

require_once (__DIR__.'\simple_html_dom.php');
require_once (__DIR__.'\..\Models\MasterThesisModel.php');

class ScraperService {

    private $url = 'http://stup.ferit.hr/index.php/zavrsni-radovi/page/';
    private $html;

    function __construct()
    {
        $this->html =  new simple_html_dom();
    }

    public function getThesisArrayFromPageResults($page) {
        $this->html->load_file($this->url . $page);
        $articles = $this->getArticles();
        $thesisArray = [];
        $index = 0;
        foreach($articles as $article)
        {
            $model = new MasterThesisModel();
            $model->title = $this->getTitle($article);
            $model->link = $this->getLinkToThesis($article);
            $model->text = $this->getText($article, $model->link);
            $model->company_oib = $this->getCompanyOib($article);
            $model->id = uniqid();

            $thesisArray[$index] = $model;
            $index++;
        }
        return $thesisArray;
    }

    private function getArticles() {
        return $this->html->find('article');
    }

    private function getTitle($article) {
        $title = $article->find('h2.[class=blog-shortcode-post-title entry-title] a', 0);
        return $title->plaintext;
    }

    private function getText($article, $url) {
        $html =  new simple_html_dom();
        $html->load_file($url);
        $text = $html->find('div.[class=post-content]', 0);
        return $text->plaintext;
    }

    private function getLinkToThesis($article) {
        $element = $article-> find('h2.[class=blog-shortcode-post-title entry-title] a', 0);
        return $element->href;
    }

    private function getCompanyOib($article) {
        $img = $article->find('img', 0);
        $src = $img->src;
        $lastIndex = strpos($src, '.png');
        return substr($src, 38, $lastIndex-38);
    }
}