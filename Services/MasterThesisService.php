<?php

require_once 'IThesisService.php';
require_once(__DIR__.'\..\Models\MasterThesisModel.php');
require_once (__DIR__.'\..\Repository\MasterThesisRepository.php');
require_once(__DIR__.'\ScraperService.php');


class MasterThesisService implements IThesisService {

    private $pageToConst = 6;
    private $scraper;
    private $repository;

    function __construct()
    {
        $this->scraper = new ScraperService();
        $this->repository = new MasterThesisRepository();
    }

    public function create() {
        for($page = 2; $page<=$this->pageToConst; $page++) {
            $this->repository->insert($this->scraper->getThesisArrayFromPageResults($page));
        }
    }

    public function read() {
        $array = $this->repository->fetchAll();
        foreach($array as $t) {
            echo $t->title, PHP_EOL;
        }
    }
}
