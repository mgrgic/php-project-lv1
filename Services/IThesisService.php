<?php

require_once(__DIR__.'\..\Models\MasterThesisModel.php');

interface IThesisService {
    public function create();
    public function read();
}

