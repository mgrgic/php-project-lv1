<?php
require_once(__DIR__.'\..\Models\MasterThesisModel.php');
require_once('BaseRepository.php');

class MasterThesisRepository extends BaseRepository {

    private $pdo;

    public function insert($thesisArray) {
        $this->pdo = $this->connect();
        $r = $this->pdo->prepare("INSERT INTO `master_theses`(`id`, `title`, `text`, `link`, `company_oib`) VALUES (:id, :title, :text, :link, :company_oib)");

        foreach($thesisArray as $t) {
            $r->execute([':id' => $t->id, ':title' => $t->title, ':text' => $t->text, ':link' => $t->link, ':company_oib' => $t->company_oib]);
        }

        unset($this->pdo);
    }

    public function fetchAll() {
        $array = [];

        $query = "SELECT * FROM master_theses";
        $this->pdo = $this->connect();
        $request = $this->pdo->query($query);
        $request->setFetchMode(PDO::FETCH_NUM);

        while ($row = $request->fetch()) {
            $item = new MasterThesisModel();
            $item->id = $row[0];
            $item->title = $row[1];
            $item->text = $row[2];
            $item->link = $row[3];
            $item->company_oib = $row[4];
            array_push($array, $item);
        }

        unset($this->pdo);
        return $array;
    }
}