<?php
class BaseRepository {
    public function connect(): PDO {
        $pdo = null;
        try {
            $pdo = new PDO('mysql:dbname=theses;host=localhost', 'root', '');
            return $pdo;
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
        return $pdo;
    }
}